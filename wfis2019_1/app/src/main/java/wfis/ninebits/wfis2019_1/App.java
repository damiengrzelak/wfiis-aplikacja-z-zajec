package wfis.ninebits.wfis2019_1;

import android.app.Application;

import com.raizlabs.android.dbflow.config.FlowConfig;
import com.raizlabs.android.dbflow.config.FlowManager;

import wfis.ninebits.wfis2019_1.retrofit.Rest;

public class App extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Rest.init();
        FlowManager.init(new FlowConfig.Builder(this).build());
    }
}
