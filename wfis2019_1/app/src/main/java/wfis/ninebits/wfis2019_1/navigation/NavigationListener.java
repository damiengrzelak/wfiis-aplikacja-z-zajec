package wfis.ninebits.wfis2019_1.navigation;

import android.support.v4.app.Fragment;
//1

public interface NavigationListener {
    void changeFragment(Fragment fragment,Boolean addToBackStack);
}
