package wfis.ninebits.wfis2019_1.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import wfis.ninebits.wfis2019_1.R;
import wfis.ninebits.wfis2019_1.base.BaseFragment;
import wfis.ninebits.wfis2019_1.models.Post;
import wfis.ninebits.wfis2019_1.retrofit.Rest;

public class FragmentA extends BaseFragment implements View.OnClickListener {

    private AppCompatButton sendButton;
    private EditText editText;

    public static FragmentA newInstance() {
        return new FragmentA();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_a, container, false);

        findViews(rootView);
        setListeners();
        getData();
        return rootView;
    }
    private void getData() {
        Rest.getRest().getPosts().enqueue(new Callback<List<Post>>() {
            @Override
            public void onResponse(Call<List<Post>> call, Response<List<Post>> response) {
                if(response.isSuccessful() && response.body() != null){
                    for(int i = 0; i < response.body().size(); i++){
                        Log.d("Item:" + i +": ", response.body().get(i).getTitle());
                    }
                }
            }

            @Override
            public void onFailure(Call<List<Post>> call, Throwable t) {

            }
        });
    }
    private void findViews(View view) {
        sendButton = view.findViewById(R.id.fragment_a_send_button);
        editText = view.findViewById(R.id.fragment_a_edit_text);
    }

    private void setListeners() {
        sendButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fragment_a_send_button:
                if (checkEditText()) {
                 //   getNavigationsInteractions().changeFragment(FragmentB.newInstance(editText.getText().toString()), true);
                } else {
                    Toast.makeText(getContext(), "Wpisz tekst", Toast.LENGTH_SHORT).show();
                }
                break;
        }

    }

    private boolean checkEditText() {
        String text = editText.getText().toString();
        return !text.isEmpty();
    }

}

