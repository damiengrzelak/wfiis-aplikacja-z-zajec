package wfis.ninebits.wfis2019_1.models;

import com.google.gson.annotations.SerializedName;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

import wfis.ninebits.wfis2019_1.database.AppDatabase;

@Table(database = AppDatabase.class)
public class Post extends BaseModel {
    @SerializedName("userId")
    @Column
    private Long userId;
    @SerializedName("id")
    @Column
    @PrimaryKey
    private Long id;
    @SerializedName("title")
    @Column
    private String title;
    @SerializedName("body")
    @Column
    private String body;


    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}
