package wfis.ninebits.wfis2019_1.database;

import com.raizlabs.android.dbflow.annotation.Database;

@Database(name = AppDatabase.NAME, version = AppDatabase.VESION)
public class AppDatabase {
    public static final String NAME = "AppDataBase";
    public static final int VESION = 1;
}
