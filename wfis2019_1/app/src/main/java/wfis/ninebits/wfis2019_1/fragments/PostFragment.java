package wfis.ninebits.wfis2019_1.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.raizlabs.android.dbflow.sql.language.SQLite;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import wfis.ninebits.wfis2019_1.R;
import wfis.ninebits.wfis2019_1.base.BaseFragment;
import wfis.ninebits.wfis2019_1.fragments.adapters.PostAdapter;
import wfis.ninebits.wfis2019_1.models.Post;
import wfis.ninebits.wfis2019_1.retrofit.Rest;

import java.util.ArrayList;
import java.util.List;

public class PostFragment extends BaseFragment implements View.OnClickListener {
    private RecyclerView recyclerView;
    private List<Post> postList = new ArrayList<>();
    private PostAdapter adapter;
    private Button refresData;

    public static PostFragment newInstance() {
        return new PostFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_post,
                container, false);

        findViews(rootView);
        setListeners();
        setAdapter();
        getDataFromDataBase();
        if (postList.size() == 0) {
            Toast.makeText(getContext(),
                    "Pobieranie danych z API, ponieważ baza danych jest pusta!",
                    Toast.LENGTH_SHORT).show();
            fetchData();
        } else {
            Toast.makeText(getContext(),
                    "Pobrano dane z bazy danych",
                    Toast.LENGTH_SHORT).show();
        }
        return rootView;
    }

    private void fetchData() {
        Toast.makeText(getContext(),
                "Pobieranie danych z API",
                Toast.LENGTH_SHORT).show();
        Rest.getRest().getPosts().enqueue(new Callback<List<Post>>() {
            @Override
            public void onResponse(Call<List<Post>> call, Response<List<Post>> response) {
                if (response.isSuccessful() && response.body() != null) {

                    for (Post post : response.body()) {
                        Post p = new Post();
                        p.setUserId(post.getUserId());
                        p.setId(post.getId());
                        p.setTitle(post.getTitle());
                        p.setBody(post.getBody());
                        p.save();
                    }
                    postList.addAll(response.body());
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<List<Post>> call, Throwable t) {

            }
        });
    }

    private void findViews(View rootView) {
        recyclerView = rootView.findViewById(R.id.post_recycler_view);
        refresData = rootView.findViewById(R.id.refresh_button);
    }

    private void setListeners() {
        refresData.setOnClickListener(this);
    }

    private void setAdapter() {
        adapter = new PostAdapter(postList);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);
    }

    private void getDataFromDataBase() {
        postList.addAll(SQLite.select().from(Post.class).queryList());
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.refresh_button:
                fetchData();
                break;
        }
    }
}
