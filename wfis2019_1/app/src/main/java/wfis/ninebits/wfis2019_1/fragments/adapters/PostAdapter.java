package wfis.ninebits.wfis2019_1.fragments.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import wfis.ninebits.wfis2019_1.R;
import wfis.ninebits.wfis2019_1.models.Post;

import java.util.List;

public class PostAdapter extends RecyclerView.Adapter<PostAdapter.PostViewHolder> {
    private List<Post> dataList;

    public PostAdapter(List<Post> dataList) {
        this.dataList = dataList;
    }

    @NonNull
    @Override
    public PostViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater
                .from(viewGroup.getContext())
                .inflate(R.layout.item_post, viewGroup, false);

        return new PostViewHolder(itemView);
    }

    @Override
    public int getItemCount() {
        if (dataList != null) {
            return dataList.size();
        } else {
            return 0;
        }
    }

    @Override
    public void onBindViewHolder(@NonNull PostViewHolder postViewHolder, int position) {
        Post item = dataList.get(position);

        postViewHolder.title.setText(item.getTitle());
        postViewHolder.id.setText(String.valueOf(item.getId()));
    }

    public class PostViewHolder extends RecyclerView.ViewHolder {
        public TextView title, id;

        PostViewHolder(View view) {
            super(view);
            title = view.findViewById(R.id.post_title);
            id = view.findViewById(R.id.post_id);
        }
    }

}
