package wfis.ninebits.wfis2019_1.retrofit;

import retrofit2.Call;
import retrofit2.http.GET;
import wfis.ninebits.wfis2019_1.models.Post;

import java.util.List;

public interface RestInterface {
    @GET("/posts")
    Call<List<Post>> getPosts();
}
